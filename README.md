Displays the history of votes of JIRA issues in a given project by opening the `votes.html` file with something like the following:

	file:///path/to/votes.html#project=EXAMPLE&max=10

NOTE: You will need to [disable](http://stackoverflow.com/questions/3102819/chrome-disable-same-origin-policy) same origin policy in Chrome.
